#!/bin/bash -xe

# instructions taken from: https://github.com/kylemanna/docker-openvpn

VPN_DATA="$HOME/vpn-data"

mkdir -p $VPN_DATA

IP_ADDRESS=$(curl ifconfig.io)

sudo apt update
sudo snap refresh
sudo snap install docker

sleep 10

# create basic configuration for the client
sudo docker run -v $VPN_DATA:/etc/openvpn --rm kylemanna/openvpn ovpn_genconfig -u udp://$IP_ADDRESS:3000 

# setup the PKI
sudo docker run -v $VPN_DATA:/etc/openvpn --rm -e EASYRSA_BATCH=1 kylemanna/openvpn ovpn_initpki nopass

# start the server
sudo docker run -v $VPN_DATA:/etc/openvpn -d -p 3000:1194/udp --cap-add=NET_ADMIN kylemanna/openvpn

# add new user
sudo docker run -v $VPN_DATA:/etc/openvpn --rm kylemanna/openvpn easyrsa build-client-full user1 nopass

sudo docker run -v $VPN_DATA:/etc/openvpn --rm kylemanna/openvpn ovpn_getclient user1 > /tmp/user1.ovpn

