#!/usr/bin/env bash

set -x
set -e

setup_vm() {

  COUNTRY_CODE="$1"

  VM_NAME="unknown"
  ZONE="unknown"

  if [[ ${COUNTRY_CODE} == "DE" ]]; then
    VM_NAME="vpn-de"
    ZONE="europe-west3-c"
  elif [[ ${COUNTRY_CODE} == "IT" ]]; then
    VM_NAME="vpn-it"
    ZONE="europe-west8-c"
  elif [[ ${COUNTRY_CODE} == "UK" ]]; then
    VM_NAME="vpn-uk"
    ZONE="europe-west2-c"
  elif [[ ${COUNTRY_CODE} == "US" ]]; then
    VM_NAME="vpn-us"
    ZONE="us-east4-c"
  else
    echo "Cannot use country code ${COUNTRY_CODE}"
    exit 1
  fi

  REGION="${ZONE:0:-2}"

  # gcloud auth login
  gcloud config set project cusfa-p

  gcloud beta compute instances create $VM_NAME \
    --project=cusfa-p \
    --machine-type=e2-standard-2 \
    --zone=$ZONE \
    --max-run-duration="4h" \
    --instance-termination-action=DELETE \
    --network-tier=PREMIUM \
    --network-interface="network-tier=PREMIUM,subnet=${REGION}" \
    --image=ubuntu-2004-focal-v20220927 \
    --image-project=ubuntu-os-cloud \
    --boot-disk-size=20GB \
    --boot-disk-type=pd-standard

  sleep 60

  gcloud --project=cusfa-p compute scp --zone=$ZONE setup_vpnserver.sh $VM_NAME:/tmp/setup_vpnserver.sh

  gcloud --project=cusfa-p compute ssh --zone=$ZONE $VM_NAME --command /tmp/setup_vpnserver.sh

  gcloud --project=cusfa-p compute scp --zone=$ZONE $VM_NAME:/tmp/user1.ovpn ./${VM_NAME}.ovpn

  sudo openvpn3 session-start --config ${VM_NAME}.ovpn
}

setup_vm $1 
