# Start server

To create a VPN server and connect to it run:

```bash
./setup_instance.sh DE
./setup_instance.sh IT
./setup_instance.sh UK
./setup_instance.sh US
```

# Start client

## Linux

Install the client (taken from: https://openvpn.net/cloud-docs/owner/connectors/connector-user-guides/openvpn-3-client-for-linux.html):

```bash
sudo apt install apt-transport-https
sudo wget https://swupdate.openvpn.net/repos/openvpn-repo-pkg-key.pub
sudo apt-key add openvpn-repo-pkg-key.pub
rm openvpn-repo-pkg-key.pub
source /etc/lsb-release
sudo wget -O /etc/apt/sources.list.d/openvpn3.list https://swupdate.openvpn.net/community/openvpn3/repos/openvpn3-${DISTRIB_CODENAME}.list
sudo apt update
sudo apt install -y openvpn3
```

Connect the client to the server

```bash
sudo openvpn3 session-start --config vpn-{italy,germany,uk,us}.ovpn
```
